# json_example

JSON is a good format for saving metadata for a study. I created this file from a CSV used by another lab member to capture info about acquisitions in the clinic. JSON can be read into many programs and languages and is easy to read/write as a human.
